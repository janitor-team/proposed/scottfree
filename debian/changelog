scottfree (1.14-11) unstable; urgency=medium

  * Team upload.
  * Add constant format strings. Closes: #997168.
  * Ensure the generated manpage is cleaned up.
  * Update VCS URLs.
  * Build-depend on libncurses-dev instead of libncurses5-dev.
  * Allow building without requiring root.
  * Switch to debhelper compatibility level 13.
  * Update debian/copyright.
  * Apply hardening flags when linking.
  * Standards-Version 4.6.0, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Tue, 26 Oct 2021 14:16:27 +0200

scottfree (1.14-10.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add patch from Bernhard Übelacker to fix crash when restoring
    from save-file. (Closes: #968375)

 -- Adrian Bunk <bunk@debian.org>  Sat, 17 Jul 2021 22:54:45 +0300

scottfree (1.14-10) unstable; urgency=low

  * Team upload.

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Stephen Kitt ]
  * Switch to source format 3.0 (quilt).
  * Switch to simple dh, compatibility level 10.
  * Move packaging repository to git.
  * Add ${misc:Depends} to the dependencies.
  * Clean up debian/control using cme.
  * Standards-Version 3.9.8.
  * Rewrite debian/copyright in machine-readable format.
  * Build docbook.6 from docbook.sgml.
  * Update the URL for the games, and add it as the homepage.
  * Fix a missing return value (allows building with clang, thanks to
    Nicolas Sévelin-Radiguet for the patch; Closes: #760675).
  * Document the existing patches.

 -- Stephen Kitt <skitt@debian.org>  Fri, 20 Jan 2017 12:52:21 +0100

scottfree (1.14-9) unstable; urgency=low

  [ Barry deFreese ]
  * New maintainer (Closes: #348950)
    + Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
  * Add myself to uploaders
  * Add VCS fields to control file
  * Update build system to debhelper
    + Add build-dep on debhelper
  * Make clean not ignore errors
  * Handle nostrip options (Closes: #438005)
  * Add quilt patching system
    + Move source changes to patches
  * Remove spurious losar directory
  * Bump Standards Version to 3.7.3

 -- Barry deFreese <bddebian@comcast.net>  Fri, 11 Jan 2008 09:52:55 -0500

scottfree (1.14-8) unstable; urgency=low

  * Bump to Standards-Version 3.6.2.
  * Orphan the package.

 -- Clint Adams <schizo@debian.org>  Sat, 21 Jan 2006 18:18:22 -0500

scottfree (1.14-7) unstable; urgency=low

  * Bump to Standards-Version 3.6.1.

 -- Clint Adams <schizo@debian.org>  Mon, 17 May 2004 23:32:27 -0400

scottfree (1.14-6) unstable; urgency=low

  * Update Standards-Version to 3.5.7.0.
  * Remove /usr/doc symlink.
  * No longer use debmake.

 -- Clint Adams <schizo@debian.org>  Sat, 31 Aug 2002 21:43:14 -0400

scottfree (1.14-5) unstable; urgency=medium

  * Update Standards-Version to 3.5.6.0.
  * Alter spelling in package description.  closes: bug#125341.
  * Avoid some implicit declarations by using include files.

 -- Clint Adams <schizo@debian.org>  Mon, 17 Dec 2001 18:24:30 -0500

scottfree (1.14-4) unstable; urgency=low

  * Update Standards-Version to 3.5.2.0.

 -- Clint Adams <schizo@debian.org>  Thu, 29 Mar 2001 16:22:05 -0500

scottfree (1.14-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Added Build-Depends, fixes Bug#84669.

 -- Lenart Janos <ocsi@debian.org>  Sat, 24 Feb 2001 03:53:27 +0100

scottfree (1.14-3) unstable; urgency=low

  * Upgraded to policy 3.1.1.
  * Updated location of GPL.

 -- Clint Adams <schizo@debian.org>  Tue,  9 May 2000 09:37:15 -0400

scottfree (1.14-2) unstable; urgency=low

  * Recompiled against libncurses4. closes: bug#52339.

 -- Clint Adams <schizo@debian.org>  Fri, 10 Dec 1999 17:23:16 -0500

scottfree (1.14-1) unstable; urgency=low

  * Initial Release.

 -- Clint Adams <schizo@debian.org>  Tue, 24 Mar 1998 03:35:49 -0500
